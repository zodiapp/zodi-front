import Vue from 'vue'
import Router from 'vue-router'
import store from '@/store.js'
import Index from '@/components/Index.vue'
import Login from '@/components/Login.vue'
import Secure from '@/components/Secure.vue'
import Register from '@/components/Register.vue'
import Swipe from "@/components/Swipe";

Vue.use(Router)

let router = new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/secure',
            name: 'secure',
            component: Secure,
            meta: {
                requiresAuth: true
            }
        },
        {
            path: '/swipe',
            name: 'swipe',
            component: Swipe,
            meta: {
                requiresAuth: true
            }
        },
    ]
})

router.beforeEach((to, from, next) => {
    if(to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        next('/login')
    } else {
        next()
    }
})

export default router