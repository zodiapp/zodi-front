import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

let backend_address = 'http://127.0.0.1:8000/api';

Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user: {}
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, token, user) {
            state.status = 'success'
            state.token = token
            state.user = user
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = ''
            state.token = ''
        },
    },
    actions: {
        login({commit}, user) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                axios({url: backend_address + '/auth/token/login/', data: user, method: 'POST'})
                    .then(resp => {
                        const token = resp.data.auth_token
                        localStorage.setItem('token', 'Token ' + token)
                        axios.defaults.headers.common['Authorization'] = token
                        commit('auth_success', token, user)
                        resolve(resp)
                    })
                    .catch(err => {
                        console.dir(err)
                        commit('auth_error')
                        localStorage.removeItem('token')
                        reject(err)
                    })
            })
        },
        // eslint-disable-next-line no-unused-vars
        register({commit}, user) {
            return new Promise((resolve, reject) => {
                console.dir(user)
                let user_credentials = {
                    email: user.email,
                    username: user.username,
                    password: user.password
                }
                let id = 0;
                console.log(backend_address + '/auth/users/')
                console.dir(user_credentials)
                axios({url: backend_address + '/auth/users/', data: user_credentials, method: 'POST'})
                    .then(resp => {
                        id = resp.data.id
                        console.log("assigned id: " + id)
                        resolve(resp)
                        let user_metadata = {
                            birthdate: user.birthdate,
                            preference_relationship: user.preference_relationship,
                            preference_gender: user.preference_gender
                        }
                        axios({url: backend_address + '/users/' + id + "/", data: user_metadata, method: 'PUT'})
                            .then(resp => {
                                resolve(resp)
                            })
                            .catch(err => {
                                console.dir(err)
                                reject(err)
                            })
                    })
                    .catch(err => {
                        console.dir(err)
                        reject(err)
                    })

            })
        },
        logout({commit}) {
            // eslint-disable-next-line no-unused-vars
            return new Promise((resolve, reject) => {
                commit('logout')
                axios({url: backend_address + '/auth/token/logout/', method: 'POST'})
                    // eslint-disable-next-line no-unused-vars
                    .then(resp => {
                        localStorage.removeItem('token')
                        delete axios.defaults.headers.common['Authorization']
                        resolve()
                    })
                    .catch(err => {
                        commit('auth_error', err)
                        localStorage.removeItem('token')
                        reject(err)
                    })
            })
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
    }
})