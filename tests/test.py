from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

driver = Chrome(executable_path='/home/skilaren/Downloads/chromedriver')
page = driver.get("http://localhost:8080")
assert driver.title == 'zodi-front'

# Go to register page
register_button = driver.find_element(By.XPATH, '//*[contains(text(), "Register")]')
register_button.click()
# Fill reg info
email_input = driver.find_elements(By.CSS_SELECTOR, 'input')[0]
email_input.send_keys('somemain2@email.com')
username_input = driver.find_elements(By.CSS_SELECTOR, 'input')[1]
username_input.send_keys('login_user2')
password_input = driver.find_elements(By.CSS_SELECTOR, 'input')[2]
password_input.send_keys('hardp4ssw0rd')
gender_input = driver.find_elements(By.CSS_SELECTOR, 'span.el-radio__label')[0]
gender_input.click()
birthdate_input = driver.find_elements(By.CSS_SELECTOR, 'input')[5]
birthdate_input.send_keys('1995-03-21')
pref_rel_input = driver.find_elements(By.CSS_SELECTOR, 'span.el-radio__label')[3]
pref_rel_input.click()
pref_gen_input = driver.find_elements(By.CSS_SELECTOR, 'span.el-radio__label')[5]
pref_gen_input.click()
register_button = driver.find_element(By.XPATH, '//*[contains(text(), "Register")]')
register_button.click()

alert = WebDriverWait(driver, timeout=20).until(lambda d: d.find_element(By.CSS_SELECTOR, 'div[role="alert"] > p'))
assert alert.text == 'Registration successful!'

# Login
username_input = driver.find_elements(By.CSS_SELECTOR, 'input')[0]
username_input.send_keys('login_user2')
password_input = driver.find_elements(By.CSS_SELECTOR, 'input')[1]
password_input.send_keys('hardp4ssw0rd')
driver.implicitly_wait(1)
login_button = driver.find_element(By.CSS_SELECTOR, 'button[type="submit"]')
login_button.click()

swipe_window = WebDriverWait(driver, timeout=20).until(lambda d: d.find_element(By.CSS_SELECTOR, 'h1'))
assert swipe_window.text == 'Username'

print('Test succeed')
driver.close()
